provider "openstack" {
  user_name   = var.os_user
  password    = var.os_password
  auth_url    = var.os_authurl
  region      = var.os_region
}

data "openstack_compute_flavor_v2" "k8ssmall" {
  name = "k8s.small"
}

data "openstack_compute_flavor_v2" "k8smedium" {
  name = "k8s.medium"
}

data "openstack_compute_flavor_v2" "k8slarge" {
  name = "k8s.large"
}

data "openstack_images_image_v2" "ubuntu" {
  name = "ubuntu-bionic-server-cloudimg-amd64"
}

resource "openstack_compute_instance_v2" "master" {
  name            = "master.k8s.cocaine.farm"
  image_id        = data.openstack_images_image_v2.ubuntu.id
  flavor_id       = data.openstack_compute_flavor_v2.k8smedium.id
  security_groups = ["default"]
  user_data       = file("${path.module}/dist/master.yaml")
  config_drive    = true

  network {
    name = "k8s-private"
    fixed_ip_v4 = "10.100.0.10"
  }
}

resource "openstack_compute_instance_v2" "node-medium" {
  count           = 2
  name            = "medium-${count.index}.k8s.cocaine.farm"
  image_id        = data.openstack_images_image_v2.ubuntu.id
  flavor_id       = data.openstack_compute_flavor_v2.k8smedium.id
  security_groups = ["default"]
  user_data       = file("${path.module}/dist/node.yaml")
  config_drive    = true

  network {
    name = "k8s-private"
    fixed_ip_v4 = "10.100.0.2${count.index}"
  }
}

variable "os_user" {}
variable "os_password" {}
variable "os_authurl" {}
variable "os_region" {}

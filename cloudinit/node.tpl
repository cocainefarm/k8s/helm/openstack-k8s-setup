#cloud-config

password: setup

package_update: true
package_upgrade: true

write_files:
    - path: /etc/sysctl.d/99-kubernetes-cri.conf
      content: |
         net.bridge.bridge-nf-call-iptables  = 1
         net.ipv4.ip_forward                 = 1
         net.bridge.bridge-nf-call-ip6tables = 1

    - path: /etc/kubernetes/cloud-config
      content: |
        [Global]
        username={{ .openstack.username }}
        password={{ .openstack.password }}
        auth-url={{ .openstack.authUrl }}
        tenant-id={{ .openstack.tenantId }}
        domain-id={{ .openstack.domainId }}
         
{{ .openstack.plainData | indent 8 }}
        
    - path: /etc/kubernetes/kubeadm_custom.conf
      content: |
          apiVersion: kubeadm.k8s.io/v1beta1
          kind: InitConfiguration
          bootstrapTokens:
           - token: "{{ .kubeadm.token }}"
             description: "default token"
             ttl: "{{ .kubeadm.tokenTtl }}"
          nodeRegistration:
            kubeletExtraArgs:
              cloud-provider: "external"
          ---
          apiVersion: kubeadm.k8s.io/v1beta1
          kind: ClusterConfiguration
          kubernetesVersion: v{{ .kubeadm.version }}
          apiServer:
            extraArgs:
              enable-admission-plugins: PersistentVolumeLabel
            certSANs:
{{ .kubeadm.certSANs | toYAML | indent 16 }}
          controllerManager:
            extraVolumes:
            - name: "cloud-config"
              hostPath: "/etc/kubernetes/cloud-config"
              mountPath: "/etc/kubernetes/cloud-config"
              readOnly: true
              pathType: File
          networking:
            serviceSubnet: {{ .kubeadm.networking.serviceSubnet }}
            podSubnet: {{ .kubeadm.networking.podSubnet }}
          ---
          apiVersion: kubeadm.k8s.io/v1beta1
          kind: JoinConfiguration
          discovery:
            bootstrapToken:
              apiServerEndpoint: "{{ .kubeadm.apiServerEndpoint }}"
              unsafeSkipCAVerification: true
              token: "{{ .kubeadm.token }}"

    - path: /etc/default/kubelet
      content: "KUBELET_EXTRA_ARGS=--cloud-provider=external"

runcmd:
    - apt-get update
    - DEBIAN_FRONTEND=noninteractive apt-get install -yq apt-transport-https ca-certificates curl gnupg-agent software-properties-common freeipa-client
    - sudo ipa-client-install --domain {{ .ipa.domain }} --mkhomedir -U -p {{ .ipa.username }} -w {{ .ipa.password }} --hostname $(hostname -s).k8s.{{ .ipa.domain }}
    - curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    - curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
    - add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    - add-apt-repository "deb https://apt.kubernetes.io/ kubernetes-xenial main"
    - apt-get update
    - modprobe overlay
    - modprobe br_netfilter
    - sysctl --system
    - apt-get install -y containerd.io
    - mkdir -p /etc/containerd
    - containerd config default > /etc/containerd/config.toml
    - systemctl restart containerd
    - apt-get install -y kubelet={{ .kubeadm.version }}-00 kubeadm={{ .kubeadm.version }}-00 kubectl={{ .kubeadm.version }}-00
    - apt-mark hold kubelet kubeadm kubectl
    - kubeadm join --config /etc/kubernetes/kubeadm_custom.conf

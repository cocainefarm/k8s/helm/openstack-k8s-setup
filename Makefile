# -------------------------------------
#  Create a k8s cluster on openstack
# -------------------------------------

install-deps:
	curl -o ${HOME}/.local/bin/gomplate -sSL https://github.com/hairyhenderson/gomplate/releases/download/v3.5.0/gomplate_linux-amd64
	chmod +x ${HOME}/.local/bin/gomplate
	curl -o /tmp/helm.tar.gz -sSL https://get.helm.sh/helm-v2.14.1-linux-amd64.tar.gz
	tar -xf /tmp/helm.tar.gz linux-amd64/helm && mv linux-amd64/helm ${HOME}/.local/bin/helm
	rm -rf /tmp/helm.tar.gz && rmdir linux-amd64

render:
	gomplate -c .=${PWD}/env/cloudinit.yaml -f cloudinit/master.tpl -o dist/master.yaml
	gomplate -c .=${PWD}/env/cloudinit.yaml -f cloudinit/node.tpl -o dist/node.yaml
	gomplate -c .=${PWD}/env/metallb.yaml -f metallb/metallb-config.yaml -o dist/metallb-config.yaml
	gomplate -c .=${PWD}/env/ingress.yaml -f ingress/nginx-ingress.yaml -o dist/nginx-ingress.yaml
	gomplate -c .=${PWD}/env/ingress.yaml -f ingress/cert-manager.yaml -o dist/cert-manager.yaml
	gomplate -c .=${PWD}/env/ingress.yaml -f ingress/cert-manager-issuer.yaml -o dist/cert-manager-issuer.yaml

tf-vars:
	export TF_VAR_os_user=${OS_USERNAME}
	export TF_VAR_os_password=${OS_PASSWORD}
	export TF_VAR_os_authurl=${OS_AUTH_URL}
	export TF_VAR_os_region=${OS_REGION_NAME}

k8s: k8s-gitlab k8s-helm k8s-metallb k8s-ingress

k8s-gitlab:
	@kubectl apply -f gitlab/gitlab_user.yaml
	@echo '-------------------------------------------------\
  api url: \
  '
	@kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $$NF}'
	@echo '-------------------------------------------------\
  ca cert: \
  '
	@kubectl get secret $$(kubectl get secrets | grep default-token | awk '{print $$1}') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode
	@echo '-------------------------------------------------\
  token: \
  '
	@kubectl -n kube-system describe secret $$(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $$1}') | grep "token:" | awk '{print $$2}'

k8s-helm:
	kubectl apply -f helm/tiller-rbac.yaml
	helm init --service-account tiller

k8s-metallb:
	kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.7.3/manifests/metallb.yaml
	kubectl apply -f dist/metallb-config.yaml

k8s-ingress: k8s-metallb
	helm install --name nginx-ingress stable/nginx-ingress -f dist/nginx-ingress.yaml
	kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.8/deploy/manifests/00-crds.yaml
	kubectl create namespace cert-manager
	kubectl label namespace cert-manager certmanager.k8s.io/disable-validation=true
	helm repo add jetstack https://charts.jetstack.io && helm repo update
	helm install --name cert-manager --namespace cert-manager --version v0.8.0 jetstack/cert-manager -f dist/cert-manager.yaml
	#kubectl apply -f dist/cert-manager-issuer.yaml

k8s-storage:
	kubectl apply -f storage/default_storage_class.yaml
